#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls -lah --color=auto --group-directories-first'
PS1='[\u@\h \W]\$ '
alias config='/usr/bin/git --git-dir=/home/sethw/dotfiles --work-tree=/home/sethw'
